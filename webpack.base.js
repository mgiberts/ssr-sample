const path = require('path');

module.exports = {
  module: {
    rules: [
      {
        test : /\.jsx?/,
        include: [path.resolve(__dirname, "src")],
        loader: 'babel-loader',
        exclude: /node_modules/,
        options: {
          presets: [
            'react',
            'stage-0',
            [
              'env', {
                targets: {
                  browsers: [
                    'last 2 versions'
                  ]
                }
              }
            ]
          ]
        }
      }
    ]
  }
};
