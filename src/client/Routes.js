import React from 'react';
import {Route} from 'react-router-dom';
import app from './App';
import homePage from './pages/HomePage';
import usersListPage from './pages/UsersListPage';
import adminsListPage from './pages/AdminsListPage';
import notFoundPage from './pages/NotFoundPage';

export default [
  {
    ...app,
    routes: [
      {
        ...homePage,
        path: '/',
        exact: true
      },
      {
        ...usersListPage,
        path: '/users'
      },
      {
        ...adminsListPage,
        path: '/admins'
      },
      {
        ...notFoundPage
      }
    ]
  }
]
