import React from 'react';

const Home = () => {
  return (
    <div className="center-align">
      <h3>Welcome</h3>
      <p> Cool huh?! </p>
    </div>
  )
};

export default {
  component: Home
};
